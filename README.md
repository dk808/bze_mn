# BZEDGE Masternode Script

This script will not work under root so create a user with sudo privileges and run script line below. You will be prompted for the masternode privkey so have that ready. If installing before actual start of masternode block the loop at the end will have to be stopped manually by using Ctrl+c. This script will install binaries, bootsrap, daemon service, update script, and a basic firewall setup. As always use at own risk.
```
wget https://bitbucket.org/dk808/bze_mn/raw/HEAD/install.sh && bash install.sh
```
